package modelo;

public class ChangeQuantity {
    private Remedy remedy;
    private Integer newQuantity;

    public ChangeQuantity(Remedy remedy, Integer newQuantity) {
        setRemedy(remedy);
        setNewQuantity(newQuantity);
    }

    public Remedy getRemedy() {
        return remedy;
    }

    public void setRemedy(Remedy remedy) {
        this.remedy = remedy;
    }

    public Integer getNewQuantity() {
        return newQuantity;
    }

    public void setNewQuantity(Integer newQuantity) {
        this.newQuantity = newQuantity;
    }
}
