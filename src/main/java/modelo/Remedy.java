package modelo;

import java.util.Observable;

public class Remedy {
	private String name;
	private int total;
	
	public Remedy() {
		super();
	}
	
	public Remedy(String name, int total) {
		super();
		this.name = name;
		this.total = total;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	@Override
	public String toString() {
		return "nombre = " + name + ", total = " + total;
	}
}
