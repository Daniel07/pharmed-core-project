package modelo;

import java.util.HashMap;
import java.util.Observable;

public class Stock extends Observable {
    private HashMap<String, Remedy> stock;

    public Stock() {
        stock = new HashMap<String, Remedy>();
    }

    public void addRemedy(String name, int quantity) {
        int oldQuantity = 0;
        if (stock.containsKey(name)) {
            oldQuantity = stock.get(name).getTotal();
            stock.get(name).setTotal(oldQuantity + quantity);
        } else {
            this.createRemedy(name, quantity);
        }
        controlChanges(name, oldQuantity);
    }

    public void extractRemedy (String name, int quantity) {
        int oldQuantity = 0;
        if (stock.containsKey(name)) {
            Remedy remedy = stock.get(name);
            oldQuantity = remedy.getTotal();
            int total = quantity > remedy.getTotal() ? 0 : remedy.getTotal() - quantity;
            stock.get(name).setTotal(total);
        }
        controlChanges(name, oldQuantity);
    }

    public int checkQuantity (String name) throws Exception {
        if (!stock.containsKey(name)){
            throw new Exception("No existe el producto");
        }
        return stock.get(name).getTotal();
    }


    public void createRemedy(String name, int quantity) {
        Remedy remedy = new Remedy(name, quantity);
        stock.put(name, remedy);
    }

    private void controlChanges(String name, int oldQuantity) {
        Remedy remedy = stock.get(name);
        if (remedy.getTotal() != oldQuantity) {
            setChanged();
            notifyObservers(remedy);
        }
    }

    public HashMap<String, Remedy> getStock() {
        return stock;
    }
}
