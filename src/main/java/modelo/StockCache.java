package modelo;


import java.util.*;

public class StockCache implements Observer {
    private HashMap<String, List<ChangeQuantity>> changes = new HashMap<>();
    private Stock stock;
    private Status status;

    public enum Status {
        CLOSED,
        OPEN
    }

    public StockCache(Stock stock) {
        this.stock = stock;
        this.status = Status.OPEN;
    }

    public boolean contains (String object) {
        return changes.containsKey(object);
    }

    private boolean isClosed() {
        return status == Status.CLOSED;
    }

    public void openCircuit() {
        status = Status.OPEN;
    }
    public void closeCircuit() {
        status = Status.OPEN;
    }

    public int get(String key) throws Exception {
        List<ChangeQuantity> history = changes.get(key);
        if (history == null || isClosed()) {
            return stock.checkQuantity(key);// Search in Stock
        } else {
            ChangeQuantity lastChange = history.get(history.size() - 1);
            return lastChange.getNewQuantity();
        }

    }

    public ChangeQuantity put(String key, ChangeQuantity value) {
        List<ChangeQuantity> history = changes.get(key);
        if (history == null) {
            changes.put(key, new ArrayList<ChangeQuantity>());
        }
        changes.get(key).add(value);
        // Check purge logic
        if (value.getNewQuantity() <= 0) {
            clearHistory(key);
        }
        if (changes.size() == 100) {
            clearAllHistory();
        }
        return value;
    }

    public List<ChangeQuantity> getHistory(String key) {
        List<ChangeQuantity> history = changes.get(key);
        return history;
    }

    public void clearHistory(String key) {
        if (this.contains(key)) {
            changes.put(key, null);
        }
    }

    public void clearAllHistory() {
        changes = new HashMap<>();
    }

    public int countChanges() {
        return changes.size();
    }

    @Override
    public void update(Observable observable, Object o) {
        Remedy remedy = (Remedy) o;
        this.put(remedy.getName(), new ChangeQuantity(remedy, remedy.getTotal()));
    }



}
