package modelo;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class StockCacheTest {
    protected Stock stock;
    protected StockCache cache;

    @Before
    public void initStock() {
        stock = new Stock();
        cache = new StockCache(stock);
        stock.addObserver(cache);
    }

    @Test
    public void testCacheOk() throws Exception {
        String remedyName = "DICLOFENAC";
        stock.addRemedy(remedyName, 10);
        stock.extractRemedy(remedyName, 8);
        stock.addRemedy(remedyName, 18);
        assertEquals(20, cache.get(remedyName));
    }

    @Test
    public void testFirstChange() throws Exception {
        String remedyName = "CEFALEXINA";
        stock.addRemedy(remedyName, 50);
        stock.extractRemedy(remedyName, 10);
        assertEquals(new Integer(50), cache.getHistory(remedyName).get(0).getNewQuantity());
    }

    @Test
    public void testThreeChanges() throws Exception {
        String remedyName = "PARACETAMOL";
        stock.addRemedy(remedyName, 100);
        stock.extractRemedy(remedyName, 10);
        stock.extractRemedy(remedyName, 10);
        stock.extractRemedy(remedyName, 10);
        assertEquals(4, cache.getHistory(remedyName).size());
    }

    @Test(expected = Exception.class)
    public void testNoChanges() throws Exception {
        cache.get("FARMATRON");
    }

    @Test
    public void testNoChangesContains() throws Exception {
        assertFalse(cache.contains("FARMATRON"));
    }

    @Test
    public void testCleanHistory() throws Exception {
        String remedyName = "INSULINA";
        stock.addRemedy(remedyName, 100);
        stock.extractRemedy(remedyName, 100);
        assertNull(cache.getHistory(remedyName));
    }

    @Test
    public void testSearchStock() throws Exception {
        String remedyName = "ENTEROGERMINA";
        stock.addRemedy(remedyName, 100);
        cache.clearHistory(remedyName);
        assertEquals(100, cache.get(remedyName));
    }

    @Test
    public void testCleanAllHistory() throws Exception {
        for (int i=0; i<100;i++) {
            stock.addRemedy("MEDICAMENT " + i, 1);
        }
        assertEquals(0, cache.countChanges());
    }

    @Test(expected = Exception.class)
    public void testCloseCache() throws Exception {
        String remedyName = "NOVALGINA";
        stock.addRemedy(remedyName, 100);
        cache.clearAllHistory();
        cache.closeCircuit();
        cache.get("NOEXISTEENSTOCK");
    }

    @Test
    public void testOpenCache() throws Exception {
        cache.clearAllHistory();
        cache.closeCircuit();
        cache.openCircuit();
        assertEquals(0, cache.countChanges());
    }
}
