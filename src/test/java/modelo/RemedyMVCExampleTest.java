package modelo;

import org.junit.Test;

import static org.junit.Assert.*;

public class RemedyMVCExampleTest {

	@Test
	public void testSetGetMethod() {
		// Action
		Remedy remedy = null;
		// test
		assertNull(remedy);
		// Action
		remedy = new Remedy();
		// test
		assertNull(remedy.getName());
		assertTrue(remedy.getTotal() == 0);
		// Action
		remedy.setName("remedy test");
		remedy.setTotal(10);
		// test
		assertNotNull(remedy.getName());
		assertNotNull(remedy.getTotal());
		assertTrue(remedy.getName().equals("remedy test"));
		assertFalse(remedy.getTotal() == 20);
		
		assertTrue(remedy.toString().equals("nombre = remedy test, total = 10"));
	}
}
