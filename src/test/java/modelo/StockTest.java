package modelo;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class StockTest {

    @Test
    public void testAddRemedy() throws Exception {
        Stock stock = new Stock();
        String remedyName = "IBUPROFENO";
        stock.addRemedy(remedyName,9);
        assertTrue(stock.checkQuantity(remedyName) == 9);
    }

    @Test
    public void testExtractRemedy() throws Exception {
        Stock stock = new Stock();
        String remedyName = "KETROLAX";
        stock.addRemedy(remedyName,9);
        stock.extractRemedy(remedyName, 3);
        assertTrue(stock.checkQuantity(remedyName) == 6);
    }

    @Test
    public void testCreateRemedy() throws Exception {
        Stock stock = new Stock();
        String remedyName = "ASPIRINETAS";
        stock.createRemedy(remedyName,100);
        assertTrue(stock.checkQuantity(remedyName) == 100);
    }

    @Test
    public void testCreateExistRemedy() throws Exception {
        Stock stock = new Stock();
        String remedyName = "ASPIRINETAS";
        stock.createRemedy(remedyName,100);
        stock.addRemedy(remedyName,200);
        assertTrue(stock.checkQuantity(remedyName) == 300);
    }

    @Test(expected = Exception.class)
    public void testUnknowRemedy() throws Exception {
        Stock stock = new Stock();
        String remedyName = "CIPLOFOXAZINA";
        boolean exists = stock.checkQuantity(remedyName) == 30;
    }
}
